import 'package:flutter/material.dart';
import 'package:shop_application/constants.dart';
import 'package:shop_application/screens/home/home_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Shop Application',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        scaffoldBackgroundColor: bgColor,
        fontFamily: "Gordita",
        textTheme: const TextTheme(
          bodyText2: TextStyle(
            color: Colors.black54,
          )
        )
      ),
      home: const HomeScreen(),
    );
  }
}
